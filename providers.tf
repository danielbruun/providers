provider "vsphere" {
    #version        = "1.11.0"
    vsphere_server = "${var.vsphere_vcenter}"
    user           = "${var.vsphere_user}"
    password       = "${var.vsphere_password}"
  
    allow_unverified_ssl = "${var.vsphere_unverified_ssl}"
  }

  provider "nsxt" {
  host                     = "192.168.110.41"
  username                 = "admin"
  password                 = "default"
  allow_unverified_ssl     = true
  max_retries              = 10
  retry_min_delay          = 500
  retry_max_delay          = 5000
  retry_on_status_codes    = [429]
}

provider "helm" {
  kubernetes {
    config_path = "/path/to/kube_cluster.yaml"
  }
}
provider "kubernetes" {
  config_context_auth_info = "ops"
  config_context_cluster   = "mycluster"
}

provider "docker" {
  host = "tcp://127.0.0.1:2376/"
}

provider "vsphere" {
  user           = var.vsphere_user
  password       = var.vsphere_password
  vsphere_server = var.vsphere_server

  # If you have a self-signed cert
  allow_unverified_ssl = true
}

provider "vra7" {
  username = "${var.username}"
  password = "${var.password}"
  tenant   = "${var.tenant}"
  host     = "${var.host}"
}
